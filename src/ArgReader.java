import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ArgReader {
    String inputFileName;
    List<String> cmds, outputs;

    public ArgReader(String inputFileName) {
        this.inputFileName = inputFileName;
        readMultilineFile();
    }

    public void readMultilineFile() {
        cmds = new LinkedList<>();
        try {
            File file = new File(this.inputFileName);
            Scanner scn = new Scanner(file);
            String line;
            while (scn.hasNextLine()) {
                line = scn.nextLine();
                if (!line.equals(""))
                    cmds.add(line);
            }
            scn.close();
        } catch (FileNotFoundException e) {
            System.out.println("input Error");
            e.printStackTrace();
        }
    }

    public void runCommands(CommandRunner cr) {
        outputs = new LinkedList<>();
        String output = null;
        for (String cmd : this.cmds) {
            if (!cr.isValid(cmd)) {
                outputs.add("input error");
            } else
                output = cr.runCmd(cmd);
            if (output != null)
                outputs.add(output);
        }
    }

    public void showOutputs() {
//        for (String output : outputs) {
//            System.out.println(output);
//        }

        System.out.print(String.join("\n", outputs));
    }
}
