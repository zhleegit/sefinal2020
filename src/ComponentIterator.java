public interface ComponentIterator {
    public boolean hasNext();
    public Component next() throws Exception;
}
