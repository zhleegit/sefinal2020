create chassisA 100 500
add bus busA 20 50 chassisA
add floppy floppyA 30 40 chassisA
get chassisA 0
get chassisA 2
print chassisA
print busA
sumOfPowerConsumption chassisA
sumOfPowerConsumption busA
sumOfAdditionCost chassisA
sumOfAdditionCost busA