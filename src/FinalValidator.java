import java.util.Set;

public class FinalValidator implements Validator {
    Set commands;

    @Override
    public boolean isValid(String cmd) {
        // invalid command results in `input error`


        // is parsable
        if (!cmd.contains(" ")) {
            return false;
        }
        String[] cmdArray = cmd.split(" ");
        String operator = cmdArray[0];
        if (!commands.contains(operator)) {
            return false;
        }


        // has right parameter formats
        boolean valid = false;
        if ((operator.equals("create") && cmdArray.length == 4)) {
            valid = true;
            String name, powerConsump, addCost;

            name = cmdArray[1]; // guarantee unique

            powerConsump = cmdArray[2];
            valid = isInteger(powerConsump) || isDouble(powerConsump);

            addCost = cmdArray[3];
            valid = isInteger(addCost) || isDouble(addCost);

        } else if ((operator.equals("add") && cmdArray.length == 6)) {
            valid = true;
            String type,compName, powerConsumption,addCost,name;

            type = cmdArray[1];
            valid = type.equals("bus") || type.equals("floppy");

            compName = cmdArray[2]; // no guarantee unique

            powerConsumption = cmdArray[3];
            valid = isInteger(powerConsumption) || isDouble(powerConsumption);

            addCost = cmdArray[3];
            valid = isInteger(addCost) || isDouble(addCost);

            name = cmdArray[4]; //guarantee existence

        } else if ((operator.equals("get") && cmdArray.length == 3)) {
            valid = true;
            String name,index;

            name = cmdArray[1]; // existence guarantee

            index = cmdArray[2]; // from 0
            valid = isInteger(index);
            if(valid){
                valid = Integer.valueOf(index) >-1;
            }

        } else if ((operator.equals("print") && cmdArray.length == 2)) {
            valid = true;
            String name;

            name = cmdArray[1]; //guarantee existence

        } else if ((operator.equals("sumOfPowerConsumption") && cmdArray.length == 2)) {
            valid = true;
            String name;

            name = cmdArray[1]; //guarantee existence


        } else if ((operator.equals("sumOfAdditionCost") && cmdArray.length == 2)) {
            valid = true;
            String name;

            name = cmdArray[1];  //guarantee existence
        }

        return valid;
    }

    @Override
    public void setCommands(Set commands) {
        this.commands = commands;
    }

    public boolean isInteger(String str){
        try{
            Integer.parseInt(str);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }

    public boolean isDouble(String str){
        try {
            Double.parseDouble(str);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }



}
