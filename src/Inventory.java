import java.util.*;

public class Inventory implements CommandRunner {
    private Validator validator;
    Set<String> commands = new HashSet<>(Arrays.asList(
            "create",
            "add",
            "get",
            "print",
            "sumOfPowerConsumption",
            "sumOfAdditionCost"));
    List<Chassis> componentList = new LinkedList<>();

    Inventory(Validator validator) {
        this.validator = validator;
        this.validator.setCommands(commands);
    }


    @Override
    public Boolean isValid(String cmd) {
        return validator.isValid(cmd);
    }

    @Override
    public String runCmd(String cmd) {


        String[] cmdArray = cmd.split(" ");
        String operator = cmdArray[0];


        // has right parameter formats
        boolean valid = false;
        if ((operator.equals("create") && cmdArray.length == 4)) {
            String name, powerConsump, addCost;
            name = cmdArray[1]; // guarantee unique
            powerConsump = cmdArray[2];
            addCost = cmdArray[3];
            return create(name,
                    Integer.valueOf(powerConsump),
                    Integer.valueOf(addCost));
        } else if ((operator.equals("add") && cmdArray.length == 6)) {
            String type, compName, powerConsumption, addCost, name;

            type = cmdArray[1];
            compName = cmdArray[2]; // no guarantee unique
            powerConsumption = cmdArray[3];
            addCost = cmdArray[4];
            name = cmdArray[5]; //guarantee existence

            return add(type, compName, Integer.valueOf(powerConsumption), Integer.valueOf(addCost), name);

        } else if ((operator.equals("get") && cmdArray.length == 3)) {
            String name, index;

            name = cmdArray[1]; // existence guarantee
            index = cmdArray[2]; // from 0
            return get(name, Integer.valueOf(index));

        } else if ((operator.equals("print") && cmdArray.length == 2)) {
            String name;
            name = cmdArray[1]; //guarantee existence
            return print(name);

        } else if ((operator.equals("sumOfPowerConsumption") && cmdArray.length == 2)) {
            String name;
            name = cmdArray[1]; //guarantee existence

            return sumOfPowerConsumption(name);

        } else if ((operator.equals("sumOfAdditionCost") && cmdArray.length == 2)) {
            String name;

            name = cmdArray[1];  //guarantee existence
            return sumOfAdditionCost(name);
        }


        return "sample result";
    }

    private String create(String name, int powerConsump, int additionCost) {
        componentList.add(new Chassis(name, powerConsump, additionCost));
        return null;
    }

    private String add(String type, String compName, int powerConsumption, int addCost, String name) {

        for (Chassis component : componentList) {
            if (name.equals(component.name)) {
                if (type.equals("bus")) {
                    component.add(new Bus(compName, powerConsumption, addCost));
                } else {
                    component.add(new Floppy(compName, powerConsumption, addCost));
                }
            }
        }

        return null;
    }

    private String get(String name, int index) {

        for (Chassis component : componentList) {
            if (name.equals(component.name)) {

                return component.get(index);
            }
        }

        return name + " does not support command get";
    }

    private String print(String name) {
        for (Component component : componentList) {
            return component.print(name);
        }
        return "input error";
    }


    private String sumOfAdditionCost(String name) {
        for (Component component : componentList) {
            if (name.equals(component.name)) {
                return String.valueOf(component.getSumOfAdditionCost());
            }
        }
        return name + " does not support command sumOfAdditionCost";
    }

    private String sumOfPowerConsumption(String name) {

        for (Component component : componentList) {
            if (name.equals(component.name)) {
                return String.valueOf(component.getSumOfPowerConsumption());
            }
        }

        return name + " does not support command sumOfPowerConsumption";
    }

}
