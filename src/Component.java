public abstract class Component {
    protected int powerConsumption, addtionCost;
    protected String name;

    Component(String name, int powerConsumption , int additionCost){
        this.name = name;
        this.powerConsumption= powerConsumption;
        this.addtionCost = additionCost;
    }

    public abstract int getSumOfPowerConsumption();
    public abstract int getSumOfAdditionCost();
    public abstract String print(String name);

}
