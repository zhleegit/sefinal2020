public class SubComponent extends Component {

    SubComponent(String name, int powerConsumption, int additionCost) {
        super(name, powerConsumption, additionCost);
    }

    @Override
    public int getSumOfPowerConsumption() {
        return this.powerConsumption;
    }

    @Override
    public int getSumOfAdditionCost() {
        return this.addtionCost;
    }

    @Override
    public String print(String parent) {
        return this.name + " (" + String.valueOf(powerConsumption) + ", " + String.valueOf(addtionCost) + ")";
    }
}
