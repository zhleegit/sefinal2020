import java.util.Set;

public interface Validator {
    boolean isValid(String cmd);
    void setCommands(Set commands);
}
