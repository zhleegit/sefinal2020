import java.util.LinkedList;
import java.util.List;

public class Chassis extends Component implements  Iterable{
    List<SubComponent> components = new LinkedList<>();

    public Chassis(String name, int powerConsumption, int addtionCost) {
        super(name, powerConsumption, addtionCost);
    }

    public boolean addComponent(SubComponent component) {
        // if there is invalid condition, you can return false for success check

        components.add(component);
        return true;
    }

    @Override
    public int getSumOfPowerConsumption() {
        int sum = this.powerConsumption;
        for (Component component : components) {
            sum += component.getSumOfPowerConsumption();
        }
        return sum;

    }

    @Override
    public int getSumOfAdditionCost() {
        int sum = this.addtionCost;
        for (Component component : components) {
            sum += component.getSumOfAdditionCost();
        }
        return sum;
    }


    @Override
    public String print(String name) {
        String output = null;
        if(name.equals(this.name)){
             output = name + " (" + String.valueOf(powerConsumption) + ", " + String.valueOf(addtionCost) + ")";
            for (Component component : components) {
                output += "\n" + name + ":" + component.print(name);
            }
            return output;
        }else{
            for (Component component : components) {
                if(name.equals(component.name))
                    output =  component.print(name);
            }
        }


        return output;
    }

    public String get(int i){
        if(i<0 ||( i>components.size()-1)){
            return "Index "+String.valueOf(i)+" out of bound of "+name;
        }
        return name+":"+components.get(i).name;
    }

    public String add(SubComponent component){
        components.add(component);
        return null;
    }



    @Override
    public ComponentIterator createIterator() {
        return new ChassisIterator();
    }

    private class ChassisIterator implements ComponentIterator {
        private int ptr;

        @Override
        public boolean hasNext() {
            return ptr<components.size();
        }

        @Override
        public Component next() throws Exception {
            if(hasNext())
                return components.get(ptr++);
            else
                throw new Exception();
        }
    }
}
