public interface Iterable {
    public ComponentIterator createIterator();
}
