public interface CommandRunner {
    Boolean isValid(String cmd);
    String runCmd(String cmd);
}
